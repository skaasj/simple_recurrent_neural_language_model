import theano
import theano.tensor as T
import numpy as np

from utils import norm_gs

# sgd with gradient clipping
def sgd_gc(cost, params, lr=1.0, max_magnitude=5.0, infDecay=0.1):
    grads = T.grad(cost=cost, wrt=params)
    updates = []
    norm = norm_gs(params, grads)
    sqrtnorm = T.sqrt(norm)
    adj_norm_gs = T.switch(T.ge(sqrtnorm, max_magnitude), 
                           max_magnitude / sqrtnorm, 1.)
    for p, g in zip(params, grads):
        updates.append((p, p - lr * g * adj_norm_gs))
    return updates, norm



