import numpy as np
import theano
from collections import OrderedDict

def load_to_gpu(dt):
    s_dt = OrderedDict()
    for key in ('tr_X', 'va_X', 'te_X', 'tr_Y', 'va_Y', 'te_Y'):
        if key in dt:
            s_dt['len_'+key] = len(dt[key])
            s_dt[key] = theano.shared(np.asarray(dt[key], dtype=np.int64), 
                                      borrow=True)
    return s_dt


def slice_batches(data_x, batch_sz):
    '''
    In the line below, we cut off some data to make (sequence, sz_batch)
    matrix. Doing this does not affect the performance in practice.
    '''
    size = (len(data_x) / batch_sz) * batch_sz  
    batch_data = data_x[:size].reshape(batch_sz, -1).transpose()
    return batch_data  # (sequence, sz_batch)


def load(datapath, trn_batch_sz=128, tst_batch_sz=32):
    
    npz_data = np.load(datapath)

    data = dict()
    data['tr_X'] = slice_batches(npz_data['train_words'][30:], trn_batch_sz)
    data['va_X'] = slice_batches(npz_data['valid_words'], tst_batch_sz)
    data['te_X'] = slice_batches(npz_data['test_words'], tst_batch_sz)
    data['n_batches'] = len(data['tr_X'])
    data['vocab_sz'] = int(npz_data['n_words'])

    return data

