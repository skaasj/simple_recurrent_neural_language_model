import numpy as np
from time import time
from collections import OrderedDict
from operator import add 

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams
from theano.tensor.shared_randomstreams import RandomStreams

from utils import Parameters, dropout, one_hot, softmax 
from optimizers import sgd_gc
import ptb


def shared(X):
    return theano.shared(np.asarray(X, dtype=theano.config.floatX))


def init_normal(shape, scale=1.0):
    if len(shape) == 1:
        scale_factor =  scale / np.sqrt(shape[0])
    elif len(shape) == 2:
        scale_factor =  scale / (np.sqrt(shape[0]) + np.sqrt(shape[1]))
    else:
        scale_factor =  scale / (np.sqrt(shape[1]) + np.sqrt(shape[2]))
    if len(shape) == 2:
        return shared(np.random.randn(*shape) * scale_factor)
    else:
        return shared(np.random.randn(*shape) * scale_factor)


def init_zero(shape, dtype=theano.config.floatX):
    return shared(np.zeros(shape))


def reset_states(hs):
    for state in hs.values():
        state = state * 0.0

#--------------------------------------------------------------------------

def layer_init_lstm(tp, op, pre='lstm'):
    n_gates = 4  # 3 lstm gates + 1 input 
    #tp[pre+'_W'] = init.glorot_normal((op.h_dim, op.h_dim * n_gates))
    #tp[pre+'_U'] = init.glorot_normal((op.h_dim, op.h_dim * n_gates))
    #tp[pre+'_b'] = init.glorot_normal((op.h_dim * n_gates,))
    tp[pre+'_W'] = init_normal((op.h_dim, op.h_dim * n_gates), 
                               scale=op.init_scale*1.5)
    tp[pre+'_U'] = init_normal((op.h_dim, op.h_dim * n_gates), 
                               scale=op.init_scale*1.5)
    tp[pre+'_b'] = init_normal((op.h_dim * n_gates,))
    return tp


def layer_step_lstm(X, h, c, tp, op, pre='lstm'):
    h_dim = op.h_dim
    g_on = T.dot(X, tp[pre+'_W']) + T.dot(h, tp[pre+'_U']) + tp[pre+'_b']
    i_on = T.nnet.sigmoid(g_on[:,:h_dim])
    f_on = T.nnet.sigmoid(g_on[:,h_dim:2*h_dim])
    o_on = T.nnet.sigmoid(g_on[:,2*h_dim:3*h_dim])
    c = f_on * c + i_on * T.tanh(g_on[:,3*h_dim:])
    h = o_on * T.tanh(c)
    return h, c
    

def layer_init(layer_type):
    return eval('layer_init_' + layer_type)


def layer_step(layer_type):
    return eval('layer_step_' + layer_type)


def init_tparams(op):
    # model params to train
    tp = Parameters() 
    #tp.W_emb = init.glorot_normal((op.vocab_sz, op.h_dim))
    tp.W_emb = init_normal((op.vocab_sz, op.h_dim), scale=op.init_scale)
    tp = layer_init(op.layers[0])(tp, op, pre='layer1')
    tp = layer_init(op.layers[1])(tp, op, pre='layer2')
    return tp


def build_model(op, tp, dt):
    trng = MRG_RandomStreams(1234)

    print 'Build model'
    # external tensor variables
    X = T.matrix('X', dtype='int64')
    Y = T.vector('Y', dtype='int64')
    X.tag.test_value = np.random.randn(op.seq_len, op.trn_batch_sz).\
                       astype(dtype=np.int64)

    # other shared variables 
    hs = Parameters() # hidden states
    hs.h1 = init_zero((op.trn_batch_sz, op.h_dim))
    hs.c1 = init_zero((op.trn_batch_sz, op.h_dim))
    hs.h2 = init_zero((op.trn_batch_sz, op.h_dim))
    hs.c2 = init_zero((op.trn_batch_sz, op.h_dim))

    def model(p_dropout=0.0):
        # initialize
        h0 = tp.W_emb[X] # (seq_len, trn_batch_sz, emb_sz)
        h0 = dropout(h0, p_dropout)
        h1, c1, h2, c2 = [hs.h1, hs.c1, hs.h2, hs.c2]
        cost = 0.
        for t in xrange(0, op.seq_len):
            if t >= op.warmup_sz:
                pyx = softmax(T.dot(h2, T.transpose(tp.W_emb)))
                cost += T.sum(T.nnet.categorical_crossentropy(
                                pyx, one_hot(X[t], op.vocab_sz)))

            h1, c1 = layer_step(op.layers[0])(h0[t], h1, c1, tp, op, pre='layer1')
            h1 = dropout(h1, p_dropout)
            h2, c2 = layer_step(op.layers[1])(h1,    h2, c2, tp, op, pre='layer2')
            h2 = dropout(h2, p_dropout)

        h_updates = [(hs.h1, h1), (hs.c1, c1), (hs.h2, h2), (hs.c2, c2)]
        return cost, h_updates
    
    cost, h_updates = model(p_dropout=op.p_dropout)
    tst_cost, tst_h_updates = model(p_dropout=0.0) 

    # compile --------------------
    seq_idx = T.iscalar('seq_idx')
    lr = T.fscalar('lr')
    offset = T.iscalar('offset')

    print "Compile optimizer"
    g_updates, norm_grad = eval(op.optimizer)(cost, tp.values(), lr=lr)
    
    print "Compile functions"
    f_trn = theano.function(
                inputs=[seq_idx, lr, offset], 
                outputs=cost,
                updates=g_updates + h_updates,
                givens={X:dt['tr_X'][offset + seq_idx * op.seq_len :
                                       offset + (seq_idx + 1) * op.seq_len]})
    f_val = theano.function(
                inputs=[seq_idx], 
                outputs=tst_cost,
                updates=tst_h_updates,
                givens={X:dt['va_X'][seq_idx * op.seq_len : 
                                      (seq_idx + 1) * op.seq_len]})
    f_tst = theano.function(
                inputs=[seq_idx], 
                outputs=tst_cost,
                updates=tst_h_updates,
                givens={X:dt['te_X'][seq_idx * op.seq_len :
                                      (seq_idx + 1) * op.seq_len]})
    return f_trn, f_val, f_tst, hs


def train(op, dt):
    
    dt = ptb.load_to_gpu(dt)
    tp = init_tparams(op)
    f_trn, f_val, f_tst, hs = build_model(op, tp, dt)

    print 'Training'
    lr = op.lr
    rnd_offset = np.random.permutation(op.seq_len)
    
    for epoch in range(op.n_epoch):
        begin_t = time()
        
        ################
        # training set 
        offset = rnd_offset[epoch % op.seq_len]
        n_update_per_epoch = (dt['len_tr_X'] - offset) / op.seq_len 
        n_pred_per_update = op.trn_batch_sz * (op.seq_len - op.warmup_sz)
        reset_states(hs)
        trn_cost = 0.
        for i in xrange(0, n_update_per_epoch):
            trn_cost += f_trn(i, lr, offset) 
            ## print intermediate result ----------------
            #if op.verbose and i % op.print_itv == 0:
                #print("%d/%d, %.2f" % (epoch, i, 
                            #np.exp(trn_cost / (n_pred_per_update * (i + 1)))))
        trn_cost /= n_update_per_epoch * n_pred_per_update


        ################
        # validation set 
        va_t = time()
        n_update_per_epoch = dt['len_va_X'] / op.seq_len 
        n_pred_per_update = op.tst_batch_sz * (op.seq_len - op.warmup_sz)
        reset_states(hs)
        val_cost = 0.
        for i in xrange(0, n_update_per_epoch):
            val_cost += f_val(i) 
        val_cost /= n_update_per_epoch * n_pred_per_update

        # print result ----------------
        print("%d, %.2f, %.2f, %ds (%ds)" % (epoch, 
                                             np.exp(trn_cost), 
                                             np.exp(val_cost),
                                             time() - begin_t,
                                             time() - va_t))
        # decay learning rate
        if op.lr_halflife != 0:
            lr = float(lr * np.power(0.5, 1./op.lr_halflife))

    #################
    ## test set 
    n_update_per_epoch = dt['len_te_X'] / op.seq_len 
    n_pred_per_update = op.tst_batch_sz * (op.seq_len - op.warmup_sz)
    reset_states(hs)
    tst_cost = 0.
    for i in xrange(0, n_update_per_epoch):
        tst_cost += f_tst(i) 
    tst_cost /= n_update_per_epoch * n_pred_per_update
    print 'test cost: %.2f' % tst_cost

    return tst_cost


def main():
    np.random.seed(123) 
    op = Parameters()
    with op:
        n_epoch = 13
        trn_batch_sz = 20 # for now, set trn & tst batch_sz to be the same
        tst_batch_sz = trn_batch_sz
        seq_len = 20
        warmup_sz = 1
        lr = 1.5    # 1.5
        lr_halflife = 40
        optimizer = 'sgd_gc'
        h_dim = 200
        wordemb_dim = 200
        layers = ['lstm', 'lstm']
        p_dropout = 0.0
        init_scale = 1.05
        verbose = False
        print_itv = 20
    print op.get()
    
    # data
    dt = ptb.load(datapath='./penntree.npz', 
                  trn_batch_sz=op.trn_batch_sz, tst_batch_sz=op.tst_batch_sz)

    op.vocab_sz = dt['vocab_sz']
    
    # start training
    train(op, dt)


if __name__ == '__main__':
    main()
