import os
import cPickle as pickle
import numpy as np
import sys
import inspect
from collections import OrderedDict

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams
from theano.tensor.shared_randomstreams import RandomStreams
sys.setrecursionlimit(100000)

rng = np.random.RandomState(123)
trng = MRG_RandomStreams(rng.randint(2**30))


def shared(X, name=None, dtype=theano.config.floatX):
    return theano.shared(np.asarray(X, dtype=dtype), name=name)


def init_normal(shape, scale=1.0):
    if len(shape) == 1:
        scale_factor =  scale / np.sqrt(shape[0])
    elif len(shape) == 2:
        scale_factor =  scale / (np.sqrt(shape[0]) + np.sqrt(shape[1]))
    else:
        scale_factor =  scale / (np.sqrt(shape[1]) + np.sqrt(shape[2]))
    if len(shape) == 2:
        return shared(rng.randn(*shape) * scale_factor)
    else:
        return shared(rng.randn(*shape) * scale_factor)


def init_zero(shape, dtype=theano.config.floatX):
    return shared(np.zeros(shape))


def norm_gs(tparams, grads):
    norm_gs = 0. 
    for g in grads:
        norm_gs += (g**2).sum()
    return norm_gs


def one_hot(idxs, n):
    z = T.zeros((idxs.shape[0], n))
    one_hot = T.set_subtensor(z[T.arange(idxs.shape[0]), idxs], 1)
    return one_hot


def softmax(X):
    e_x = T.exp(X - X.max(axis=1).dimshuffle(0, 'x'))
    return e_x / e_x.sum(axis=1).dimshuffle(0, 'x')


def dropout(X, p=0.):
    if p > 0:
        retain_prob = 1 - p
        X *= trng.binomial(X.shape, p=retain_prob, dtype=theano.config.floatX)
        X /= retain_prob
    return X


class Parameters():
    def __init__(self):
        #self.__dict__['tparams'] = dict()
        self.__dict__['tparams'] = OrderedDict()
    
    def __setattr__(self,name,array):
        tparams = self.__dict__['tparams']
        if name not in tparams:
            tparams[name] = array
    
    def __setitem__(self,name,array):
        self.__setattr__(name,array)
    
    def __getitem__(self,name):
        return self.__getattr__(name)
    
    def __getattr__(self,name):
        tparams = self.__dict__['tparams']
        return self.tparams[name]

    #def __getattr__(self):
        #return self.get()

    def remove(self,name):
        del self.__dict__['tparams'][name]

    def get(self):
        return self.__dict__['tparams']

    def values(self):
        tparams = self.__dict__['tparams']
        return tparams.values()

    def save(self,filename):
        tparams = self.__dict__['tparams']
        pickle.dump({p:tparams[p] for p in tparams},open(filename,'wb'),2)

    def load(self,filename):
        tparams = self.__dict__['tparams']
        loaded = pickle.load(open(filename,'rb'))
        for k in loaded:
            tparams[k] = loaded[k]

    def setvalues(self, values):
        tparams = self.__dict__['tparams']
        for p, v in zip(tparams, values):
            tparams[p] = v

    def __enter__(self):
        _,_,_,env_locals = inspect.getargvalues(inspect.currentframe().f_back)
        self.__dict__['_env_locals'] = env_locals.keys()

    def __exit__(self,type,value,traceback):
        _,_,_,env_locals = inspect.getargvalues(inspect.currentframe().f_back)
        prev_env_locals = self.__dict__['_env_locals']
        del self.__dict__['_env_locals']
        for k in env_locals.keys():
            if k not in prev_env_locals:
                self.__setattr__(k,env_locals[k])
                env_locals[k] = self.__getattr__(k)
        return True


def concatenate(tensor_list, axis=0):
    """
    Alternative implementation of `theano.T.concatenate`.
    This function does exactly the same thing, but contrary to Theano's own
    implementation, the gradient is implemented on the GPU.
    Backpropagating through `theano.T.concatenate` yields slowdowns
    because the inverse operation (splitting) needs to be done on the CPU.
    This implementation does not have that problem.
    :usage:
        >>> x, y = theano.T.matrices('x', 'y')
        >>> c = concatenate([x, y], axis=1)
    :parameters:
        - tensor_list : list
            list of Theano T expressions that should be concatenated.
        - axis : int
            the tensors will be joined along this axis.
    :returns:
        - out : T
            the concatenated T expression.
    """
    concat_size = sum(tt.shape[axis] for tt in tensor_list)

    output_shape = ()
    for k in range(axis):
        output_shape += (tensor_list[0].shape[k],)
    output_shape += (concat_size,)
    for k in range(axis + 1, tensor_list[0].ndim):
        output_shape += (tensor_list[0].shape[k],)

    out = T.zeros(output_shape)
    offset = 0
    for tt in tensor_list:
        indices = ()
        for k in range(axis):
            indices += (slice(None),)
        indices += (slice(offset, offset + tt.shape[axis]),)
        for k in range(axis + 1, tensor_list[0].ndim):
            indices += (slice(None),)

        out = T.set_subtensor(out[indices], tt)
        offset += tt.shape[axis]

    return out


